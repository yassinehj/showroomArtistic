package tn.esprit.ressources;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import tn.esprit.entities.Artist;
import tn.esprit.entities.Customer;
import tn.esprit.entities.GalleryOwner;
import tn.esprit.entities.User;
import tn.esprit.services.UserServiceLocal;
//import tn.esprit.services.UserServiceRemote;
@Stateless

@Path("/user")

public class UserRessouces {
	@EJB
	UserServiceLocal service;
	
	
	@GET
	@Produces( "application/xml")
	public List<User> findUsers(){
		return service.getUsers();
	}
	
	
	
	@POST
	@Path("add/{role}")
	@Consumes("application/xml")
	public String addUser(User u,@PathParam("role") String role) {
		
		if(service.addUser(u,role)) {
			return "ok";}
			else {return "erreur";
		}
	}

}
