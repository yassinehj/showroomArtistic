package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.entities.Artist;
import tn.esprit.entities.Customer;
import tn.esprit.entities.GalleryOwner;
import tn.esprit.entities.User;

/**
 * Session Bean implementation class UserService
 */
@Stateless
public class UserService implements UserServiceLocal {
@PersistenceContext(unitName="showroom-ejb")
EntityManager em;
    /**
     * Default constructor. 
     */
    public UserService() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public List<User> getUsers() {
		Query q=em.createQuery("select u from User u");
		return q.getResultList();
	}
//	@Override
//	public boolean addUser(User u,Artist art,,String role) {
//
////	       User u = new User();
////	        u.setId(1);
////	        u.setLastName("yassine");
//	        em.persist(u);
//	        return true;
//	   
//	}
	@Override
	public boolean addUser(User u, String role) {
		if(role.equals("Artist")) {
			Artist art=(Artist) u;
			em.persist(art);
			return true;
		}else if(role.equals("Customer")) {
			Customer c=(Customer) u;
			em.persist(c);
			return true;
		}else if(role.equals("GalleryOwner")) {
			GalleryOwner g=(GalleryOwner) u;
			em.persist(g);
			return true;
		}else {
			System.out.println(role+" n'existe pas");
		return false;
		}
		
	}
    

}
