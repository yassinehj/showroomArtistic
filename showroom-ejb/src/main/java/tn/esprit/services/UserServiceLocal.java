package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Artist;
import tn.esprit.entities.Customer;
import tn.esprit.entities.GalleryOwner;
import tn.esprit.entities.User;

@Local
public interface UserServiceLocal {
List<User> getUsers();
boolean addUser(User u,String role);
}
