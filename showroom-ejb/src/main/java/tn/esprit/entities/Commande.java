package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Double;
import java.lang.Integer;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Commande
 *
 */
@Entity
@XmlRootElement
public class Commande implements Serializable {

	   
	@Id
	private Integer idCommande;
	private Integer fk_User;
	private Integer fk_Article;
	private Integer nbrArticle;
	private Double prixTotal;
	private Integer fk_Facture;
	private Date dateCommande;
	@ManyToOne
	@JoinColumn(name="customers")
	private Customer customers ;
	private static final long serialVersionUID = 1L;

	public Commande() {
		super();
	}   
	public Integer getIdCommande() {
		return this.idCommande;
	}

	public void setIdCommande(Integer idCommande) {
		this.idCommande = idCommande;
	}   
	public Integer getFk_User() {
		return this.fk_User;
	}

	public void setFk_User(Integer fk_User) {
		this.fk_User = fk_User;
	}   
	public Integer getFk_Article() {
		return this.fk_Article;
	}

	public void setFk_Article(Integer fk_Article) {
		this.fk_Article = fk_Article;
	}   
	public Integer getNbrArticle() {
		return this.nbrArticle;
	}

	public void setNbrArticle(Integer nbrArticle) {
		this.nbrArticle = nbrArticle;
	}   
	public Double getPrixTotal() {
		return this.prixTotal;
	}

	public void setPrixTotal(Double prixTotal) {
		this.prixTotal = prixTotal;
	}   
	public Integer getFk_Facture() {
		return this.fk_Facture;
	}

	public void setFk_Facture(Integer fk_Facture) {
		this.fk_Facture = fk_Facture;
	}   
	public Date getDateCommande() {
		return this.dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}
	public Customer getCustomers() {
		return customers;
	}
	public void setCustomers(Customer customers) {
		this.customers = customers;
	}
	
   
}
