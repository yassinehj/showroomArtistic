package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import tn.esprit.entities.User;

/**
 * Entity implementation class for Entity: GalleryOwner
 *
 */
@Entity
@DiscriminatorValue("GalleryOwner")
@XmlRootElement
public class GalleryOwner extends User implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public GalleryOwner() {
		super();
	}
   
}
