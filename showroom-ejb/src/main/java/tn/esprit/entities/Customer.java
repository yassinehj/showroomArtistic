
package tn.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import tn.esprit.entities.User;

/**
 * Entity implementation class for Entity: Customer
 *
 */
@Entity
@XmlRootElement
@DiscriminatorValue("Customer")
public class Customer extends User implements Serializable {

	
	private String numTel;
	@ManyToMany
	@JoinTable(name="Participate")/*
	 * , joinColumns=@JoinColumn(name="idEventFK", referencedColumnName="idEvent"), 
	inverseJoinColumns=@JoinColumn(name="idCustomerFK",referencedColumnName="id"))*/
	private List<Event> events;
	@OneToMany(mappedBy="customers",fetch=FetchType.LAZY)
	private List<Commande> commandes ;
	private static final long serialVersionUID = 1L;

	public Customer() {
		super();
	}   
	public String getNumTel() {
		return this.numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}
	
	
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
   
}
