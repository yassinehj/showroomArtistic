package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

import javax.xml.bind.annotation.XmlRootElement;

import tn.esprit.entities.User;

/**
 * Entity implementation class for Entity: Administrateur
 *
 */
@Entity
@DiscriminatorValue("Administrateur")
@XmlRootElement

public class Administrateur extends User implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Administrateur() {
		super();
	}
   
}
