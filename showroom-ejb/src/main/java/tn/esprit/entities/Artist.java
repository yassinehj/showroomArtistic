package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import tn.esprit.entities.User;

/**
 * Entity implementation class for Entity: Artist
 *
 */
@Entity
@DiscriminatorValue("Artist")
@XmlRootElement
public class Artist extends User implements Serializable {

	private String field;
	/*@OneToMany(mappedBy="artist")
	private List<Event> events ;*/
	@OneToMany(mappedBy="artists",fetch=FetchType.LAZY)
	private List<Article> articles ;
	private static final long serialVersionUID = 1L;

	public Artist() {
		super();
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	/*public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
   */
}
