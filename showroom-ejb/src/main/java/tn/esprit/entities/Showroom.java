package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Showroom
 *
 */
@Entity
@XmlRootElement
public class Showroom implements Serializable {

	   
	@Id
	private Integer idShowroom;
	private String adress;
	private String surface;
	private String description;
	private String equipmentIncluded;
	@OneToMany(mappedBy="showroom")
	private List<Event> events ;
	@OneToMany(mappedBy="showroom")
	private List<Article> articles ;
	private static final long serialVersionUID = 1L;

	public Showroom() {
		super();
	}   
	public Integer getIdShowroom() {
		return this.idShowroom;
	}

	public void setIdShowroom(Integer idShowroom) {
		this.idShowroom = idShowroom;
	}   
	public String getAdress() {
		return this.adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}   
	public String getSurface() {
		return this.surface;
	}

	public void setSurface(String surface) {
		this.surface = surface;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public String getEquipmentIncluded() {
		return this.equipmentIncluded;
	}

	public void setEquipmentIncluded(String equipmentIncluded) {
		this.equipmentIncluded = equipmentIncluded;
	}
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
   
}
