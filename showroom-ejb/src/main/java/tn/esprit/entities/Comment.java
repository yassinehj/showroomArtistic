package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Comment
 *
 */
@Entity
@XmlRootElement

public class Comment implements Serializable {

	   
	@Id
	private Integer idComment;
	private Integer idUser;
	private Integer idPost;
	private String description;
	@ManyToOne
	@JoinColumn(name="user")
	private User user ;
	@ManyToOne
	@JoinColumn(name="post")
	private Post post;
	private static final long serialVersionUID = 1L;

	public Comment() {
		super();
	}   
	public Integer getIdComment() {
		return this.idComment;
	}

	public void setIdComment(Integer idComment) {
		this.idComment = idComment;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	public Integer getIdPost() {
		return idPost;
	}
	public void setIdPost(Integer idPost) {
		this.idPost = idPost;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
   
}
